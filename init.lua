-- Settings
require("nick.core")
require("nick.core.filebuff")
require("nick.core.ui")
require("nick.core.environment")

-- Keybindings
require("nick.core.keybindings")

--Plugins
require("nick.plugin")
